import Vue from 'vue'
import Vuex from 'vuex'

import auth from '@/store/modules/auth'
import student from '@/store/modules/student'
import grade from '@/store/modules/grade'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        drawer: true,
        isLoading: true,
        tasks: JSON.parse(localStorage.getItem('tasks') || '[] ') // 
    },

    mutations: {
        toggleDrawer(state) { state.drawer = !state.drawer },
        setIsLoading(state, flag) { state.isLoading = flag },
        createTask(state, task) { //
            state.tasks.push(task) //добавляем

            localStorage.setItem('tasks', JSON.stringify(state.tasks)) // сохраняем 
        }
    },

    actions: {
        createTask({ commit }, task) { //
            commit('createTask', task)
        }
    },

    modules: {
        auth,
        student,
        grade
    },
    getters: { //
        tasks: s => s.tasks
    }
})